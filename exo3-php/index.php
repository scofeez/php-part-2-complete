<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
      <?php
          $age = 17;
          $genre = "femme";

          if (($age >= 18) && ($genre == "homme")) {
            echo "Vous êtes un homme et vous êtes majeur";
          }
          elseif (($age < 18) && ($genre == "homme")) {
            echo "Vous êtes un homme et vous êtes mineur";
          }
          elseif (($age >= 18) && ($genre == "femme")) {
            echo "Vous êtes une femme et vous êtes majeur";
          }
          elseif (($age < 18) && ($genre == "femme")) {
            echo "Vous êtes une femme et vous êtes mineur";
          }
          // si une erreur se produit lors de la saisie du genre ou de l'age..
          else {
            echo "Vous êtes quoi ? Réponse: un cactus";
          }
      ?>
  </body>
</html>
